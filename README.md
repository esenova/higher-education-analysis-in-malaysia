# **HIGHER EDUCATION ANALYSIS IN MALAYSIA**

## *Abstract*

Higher education in Malaysia is offered by both public and private universities, as well as colleges. The Ministry of Higher Education regulates and oversees the higher education system in the country. 

The Malaysian government has also implemented various initiatives to improve the quality of higher education in the country. These initiatives include the development of research and innovation, the enhancement of international collaboration, and the promotion of entrepreneurship among graduates. 

Overall, higher education in Malaysia is continuously evolving to meet the demands of a rapidly changing world, with a strong emphasis on providing students with the skills and knowledge necessary for success in the global market.

# **GRAPH 1**

![Graph 1](Graphfiles/graph1.png)

### *Visualization purpose:*

The graph shows the number of academic staff at private higher education institutions over the years from 2002. The graph starts at a relatively low point in 2002 with around 1000-5000 academic staff and gradually increases over the years. By 2020, the number of academic staff reaches a peak of around 20,000, indicating a significant increase in the number of academic staff working at private higher education instituitons. 

### *Visualization tone:*

The graph is presented in a professional and informative tone, reflecting the significance of the data being presented. The use of clear and concise line charts, with distinct data points and labeled axes, conveys a sense of precision and accuracy in the representation of the data. The choice of colors also adds to the visual appeal of the graph and draws attention to the key trends and patterns in the data. 

### *Visualization experience :*

The visualization of the number of academic staff per year graph is an exhibitory experience, showcasing the key trends and patterns in the data. The graph presents a clear picture of the growth of academic staff in private higher education institutions over the years, making it an effective tool for stakeholders in the education industry to understand the evolution of the sector. The use of line charts with clear data points and labeled axes provides an intuitive representation of the data, allowing the viewer to easily understand the patterns and trends in the data. 

### *Data modelling used:*

The data modelling used for the line graph of the number of academic staff per year is simple linear representation of the data, using line charts to showcase the growth pattern of the data over time. The data was modelled using a time-series approach, where the data was collected and organized in a manner that captured the evolution of the number of academic staff in private higher education institutions over the years. 

### *Data transformation performed:*

The data transformation performed for the line graph of the number of academic staff per year involved cleaning, creating, and consolidating the data to ensure its accuracy and relevance. The data was first collected from various sources and then cleaned to remove any duplicate or irrelevant data points. 

### *Data visualization:*

The data visualization for the line graph of the number of academic staff per year involved the selection of an appropriate graphical representation of the information and data. The line chart was selected as the preferred graphical representation of the data as it is an effective tool for showcasing the growth pattern of data over time. 

# **GRAPH 2**

![Graph 2](Graphfiles/graph2.png)

### *Visualization purpose:*

The purpose of the visualization of the number of lecturers in higher education institutions graph is to showcase the growth pattern of academic staff in the education sector over time. The graph provides an accurate representation of the number of lecturers in higher education institutions, making it an important tool for stakeholders in the education industry to understand the growth and development of the sector.

### *Visualization tone:*

The tone of the visualization for the number of lecturers in higher education institutions is informative and objective. The goal is to clearly communicate the data and patterns in the graph, rather than evoke a specific emotion or feeling. The use of colors, labels, and other visual elements are meant to enhance the clarity and understanding of the information presented. 

### *Visualization experience*

The visualization experience for the data on the number of lecturers in higher education institutions is explanatory. The goal is to provide a clear and straightforward explanation of the data, showing the patterns and trends in a way that is easy to understand. The design elements, such as axis labels, annotations, and legends are used to support the explanation and interpretation of the data. 

### *Data modelling used:*

The type of data modeling used for the bar chart graph on the number of lecturers in higher education institutions is likely a simple aggrregation of data. This type of data modeling involves grouping the data by certain categories, such as higher education institutions, and then calculating the total number of lecturers for each group. The resulting aggregated data is then used to create the bar chart visualization. 

### *Data transformation performed:*

The data transformation performed for the bar graph on the number of lecturers in higher education institutions may involve a few steps, including cleaning, creating, and consolidating.

### *Data visualization:*

For the bar graph on the number of lecturers in higher education institutions uses color combinations of purple tones to distinguish between male and female lecturers. The female lecturers are represented by a pinkish purple color, while the male lecturers are represented by a deep purple color. This choice of color helps to quickly and easily distinguish between the two groups and supports the interpretation of the data.

# **GRAPH 3**

![Graph 3](Graphfiles/graph3.png)

### *Visualization purpose*

Visualization of the number of students and lecturers in higher education can serve various purposes:
- Curiosity motivation: Graphs and charts can make complex data more accessible and understandable to the general public, making it easier to see trends, patterns and anomalies. This can spark curiosity and encourage further exploration and investigation.
- Planning and decision making: Education administration can use this visualization to assess the current and projected number of students and lecturers, helping them to make informed decisions about resource allocation and future planning.
- Comparison and benchmarking: The data can be compared to other educational institutions, both nationally and internationally, to see how they are performing relative to their peers, and to identify areas for improvement.

### *Visualization tone*

The tone of the visualization should be neutral, objective and informative. The goal is to present the data accurately and clearly, without introducing any personal bias or subjective interpretation. The tone should be straightforward, clear and easy to understand, allowing the reader to draw their own conclusions based on the data presented.

### *Visualization experience:*

A line bar chart graph is an example of an explanatory visualization. An explanatory visualization is used to convey information and explain complex data to the reader. The focus is on presenting the data in a clear, straightforward and easy-to understand format, allowing the reader to see relationships in the data.

### *Data visualization:*

The graph uses a combination of colors, bars, and lines to effectively communicate the data.

The green line represents the number of lecturers and the orange and yellow bars represent the number of students, with the bars divided into two by gender. The use of green and yellow tones together creates a clear visual distinction between the number of lecturers and the number of students, making it easy for the reader to understand the data.



